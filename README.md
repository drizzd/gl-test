# Diagram Render Engines

## PlantUML

```plantuml
Bob -> Alice : hello
Alice -> Bob : Go Away
```

## Mermaid

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```